#!/bin/bash

#author: benmaxrei
#date:   04.01.2023

GR='\e[32m\e[1m' #Green
YL='\e[33m\e[1m' #Yellow
GY='\e[39m\e[1m' #Gray
NC='\e[0m'       #No Color

if (($# < 1)) || [ "$1" == "help" ]; then
	echo -e "${GR}usage:${NC}\n\t${YL}inkscape needed!!${NC}\n${GY}\t$0${NC} ${YL}[OUTPUT_FOLDER]${NC}"
	exit 1
fi

OUTPUT_FOLDER=$1

for FILE in *; do
	extension="${FILE##*.}"
	filename="${FILE%.*}"
	if [ "$extension" == "svg" ]; then
		echo -en "${YL}convert: $FILE ... ${NC}"
		inkscape -f "$FILE" -e "$OUTPUT_FOLDER/${filename}.png" -C -w 16 -h 16 >/dev/null 2>&1
		cp "${OUTPUT_FOLDER}/$filename.png" "${OUTPUT_FOLDER}/${filename}16x16.png" >/dev/null 2>&1
		inkscape -f "$FILE" -e "${OUTPUT_FOLDER}/${filename}24x24.png" -C -w 24 -h 24 >/dev/null 2>&1
		echo -e "${GR}done${NC}"
	fi
done
